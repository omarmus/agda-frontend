'use strict';
const merge = require('webpack-merge');
const prodEnv = require('./prod.env');

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_URL: '"http://localhost:3300/api/"',
  GRAPHQL_URL: '"http://localhost:3300/graphql/"',
  AUTH_URL: '"http://localhost:3300/auth"',
  ONBEFOREUNLOAD: false,
  DEBUG_MODE: true
});
