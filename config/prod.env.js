'use strict';
module.exports = {
  NODE_ENV: '"production"',
  API_URL: '"https://produccion.com.bo/api/"',
  GRAPHQL_URL: '"https://produccion.com.bo/graphql/"',
  AUTH_URL: '"https://produccion.com.bo/auth"',
  TIME_SESSION_EXPIRED: 30,
  ONBEFOREUNLOAD: false,
  AUTH_TOKEN: '"Bearer"',
  LANG: '"es"',
  DEBUG_MODE: false
};
