export default {
  clientes: 'Entities',
  configuracion: 'Settings',
  usuarios: 'Users',
  modulos: 'Módulos y permisos',
  roles: 'Roles',
  parametros: 'Preferencias',
  logs: 'Logs del sistema',
  config: 'Configuraciones'
};
