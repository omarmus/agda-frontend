export default {
  clientes: 'Clientes',
  configuracion: 'Configuraciones',
  usuarios: 'Usuarios',
  modulos: 'Módulos y permisos',
  roles: 'Roles',
  parametros: 'Preferencias',
  logs: 'Logs del sistema',
  config: 'Configuraciones'
};
