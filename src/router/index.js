import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/admin/Dashboard';
import AppNotFound from '@/common/layout/pages/AppNotFound';
import AppForbidden from '@/common/layout/pages/AppForbidden';
import AppError from '@/common/layout/pages/AppError';

// System
import Login from '@/components/admin/auth/Login';
import Account from '@/components/admin/account/Account';
import Cliente from '@/components/admin/cliente/Cliente';
import Usuario from '@/components/admin/usuario/Usuario';
import Modulo from '@/components/admin/modulo/Modulo';
import Preferencias from '@/components/admin/preferencias/Preferencias';
import Log from '@/components/admin/Log';

// AGDA
import DetalleCliente from '@/components/admin/cliente/DetalleCliente';
import DocumentoPlantilla from '@/components/agda/documento/DocumentoPlantilla';
import Operaciones from '@/components/agda/operacion/Operaciones';
import DetalleOperacion from '@/components/agda/operacion/DetalleOperacion';
import Cuentas from '@/components/agda/cuenta/Cuenta';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/usuarios',
      name: 'Usuario',
      component: Usuario
    },
    {
      path: '/account',
      name: 'Account',
      component: Account
    },
    {
      path: '/clientes',
      name: 'Cliente',
      component: Cliente
    },
    {
      path: '/documentos',
      name: 'DocumentoPlantilla',
      component: DocumentoPlantilla
    },
    {
      path: '/operaciones',
      name: 'Operaciones',
      component: Operaciones
    },
    {
      path: '/cuentas',
      name: 'Cuentas',
      component: Cuentas
    },
    {
      path: '/operacion/:idOperacion',
      name: 'DetalleOperacion',
      component: DetalleOperacion
    },
    {
      path: '/cliente/:idCliente',
      name: 'DetalleCliente',
      component: DetalleCliente
    },
    {
      path: '/parametros',
      name: 'Preferencias',
      component: Preferencias
    },
    {
      path: '/modulos',
      name: 'Módulo',
      component: Modulo
    },
    {
      path: '/logs',
      name: 'Logs',
      component: Log
    },
    {
      path: '/404',
      component: AppNotFound
    },
    {
      path: '/403',
      component: AppForbidden
    },
    {
      path: '/500',
      component: AppError
    },
    {
      path: '*',
      component: AppNotFound
    }
  ]
});
